package my.jessyan.mvpart.mvp.refresh;


import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

/**
 * author : liupeng
 * date   : 2021/4/6
 * desc   : 刷新基类
 */
public abstract class BaseRefreshLayout extends ComponentContainer {
    private final int NUM_50 = 50;
    public enum REFRESH {
        TAP_TO_REFRESH,//（未刷新）
        PULL_TO_REFRESH,// 下拉刷新
        RELEASE_TO_REFRESH,// 释放刷新
        REFRESHING,// 正在刷新
        TAP_TO_LOADMORE,// 未加载更多
        LOADING,// 正在加载
    }

    public int mPullRefreshHeight;


    public BaseRefreshLayout(Context context) {
        this(context, null);
    }

    public BaseRefreshLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public BaseRefreshLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);

        mPullRefreshHeight = DisplayUtils.vp2px(context, NUM_50);
        init();
    }

    /**
     * 初始化
     */
    public abstract void init();

    public abstract void setRefreshText(String str);

    public abstract void setStartRefresh();

    public abstract void setFinishRefresh();

    public void setRefreshState(REFRESH refresh) {
        switch (refresh) {
            case PULL_TO_REFRESH:// 下拉刷新
                setRefreshText("下拉刷新");
                setStartRefresh();
                break;
            case RELEASE_TO_REFRESH:// 释放刷新
                setRefreshText("释放以刷新...");
                setStartRefresh();
                break;
            case REFRESHING:// 正在刷新
                setRefreshText("加载中...");
                setStartRefresh();
                break;
            case TAP_TO_REFRESH:// 未刷新
                setRefreshText("下拉刷新");
                setFinishRefresh();
                break;
            case LOADING:// 正在加载
                setRefreshText("加载更多");
                setStartRefresh();
                break;
            case TAP_TO_LOADMORE:// 未加载更多
                setStartRefresh();
                break;
        }
    }
}
