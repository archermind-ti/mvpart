package my.jessyan.mvpart.mvp.refresh;

import my.jessyan.mvpart.mvp.ResourceTable;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * 自定义下拉刷新的头部组件
 *
 * @author 裴云飞
 * @date 2020/12/27
 */
public class TextOverComponent extends BaseRefreshLayout {

    private Image mImage;
    private Text mText;
    private AnimatorProperty mAnimator;
    private final int NUM_700=700;
    private final int NUM_300=360;

    public TextOverComponent(Context context) {
        this(context, null);
    }

    public TextOverComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public TextOverComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    public void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_refresh_over_view, this, true);
        mImage = (Image) findComponentById(ResourceTable.Id_image);
        mText = (Text) findComponentById(ResourceTable.Id_text);
    }

    @Override
    public void setRefreshText(String str) {

    }

    @Override
    public void setStartRefresh() {
        mAnimator = mImage.createAnimatorProperty();
        mAnimator.setDuration(NUM_700).rotate(NUM_300).setLoopedCount(AnimatorProperty.INFINITE).setTarget(mImage).start();

    }

    @Override
    public void setFinishRefresh() {
        mAnimator.cancel();
    }
//
//    /**
//     * 滚动
//     *
//     * @param scrollY 纵轴滚动的距离
//     * @param pullRefreshHeight 触发下拉刷新时的最小高度
//     */
//    @Override
//    public void onScroll(int scrollY, int pullRefreshHeight) {
//
//    }
//
//    /**
//     * 显示头部组件
//     */
//    @Override
//    public void onVisible() {
//        mText.setText("下拉刷新");
//    }
//
//    /**
//     * 超出头部视图高度，松手开始加载
//     */
//    @Override
//    public void onOver() {
//        mText.setText("松开刷新");
//    }
//
//    /**
//     * 正在刷新
//     */
//    @Override
//    public void onRefresh() {
//        mText.setText("正在刷新...");
//        mAnimator = mImage.createAnimatorProperty();
//        mAnimator.setDuration(700).rotate(360).setLoopedCount(AnimatorProperty.INFINITE).setTarget(mImage).start();
//    }
//
//    /**
//     * 刷新完成
//     */
//    @Override
//    public void onFinish() {
//        mAnimator.cancel();
//    }
}
