package my.jessyan.mvpart.mvp;

import me.jessyan.art.base.BaseApplication;
import me.jessyan.art.di.module.GlobalConfigModule;
import my.jessyan.mvpart.mvp.model.api.Api;
import my.jessyan.mvpart.mvp.model.api.config.GlobalHttpHandlerImpl;
import my.jessyan.mvpart.mvp.model.api.config.ResponseErrorListenerImpl;
import my.jessyan.mvpart.mvp.slice.MainAbilitySlice;
import ohos.aafwk.content.Intent;

import java.util.concurrent.TimeUnit;

public class MainAbility extends BaseApplication {
    private final int  NUM_10=10;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        configApi();
    }

    private void configApi() {
        GlobalConfigModule.Builder builder = GlobalConfigModule.builder();
        builder.baseurl(Api.APP_DOMAIN)
                //这里提供一个全局处理 Http 请求和响应结果的处理类, 可以比客户端提前一步拿到服务器返回的结果, 可以做一些操作, 比如 Token 超时后, 重新获取 Token
                .globalHttpHandler(new GlobalHttpHandlerImpl(this.getContext()))
                //用来处理 RxJava 中发生的所有错误, RxJava 中发生的每个错误都会回调此接口
                //RxJava 必须要使用 ErrorHandleSubscriber (默认实现 Subscriber 的 onError 方法), 此监听才生效
                .responseErrorListener(new ResponseErrorListenerImpl())
                .retrofitConfiguration((context1, retrofitBuilder) -> {//这里可以自己自定义配置 Retrofit 的参数, 甚至您可以替换框架配置好的 OkHttpClient 对象 (但是不建议这样做, 这样做您将损失框架提供的很多功能)
//                    retrofitBuilder.addConverterFactory(FastJsonConverterFactory.create());//比如使用 FastJson 替代 Gson
                })
                .okhttpConfiguration((context1, okhttpBuilder) -> {//这里可以自己自定义配置 Okhttp 的参数
//                    okhttpBuilder.sslSocketFactory(); //支持 Https, 详情请百度
                    okhttpBuilder.writeTimeout(NUM_10, TimeUnit.SECONDS);
                })
                .rxCacheConfiguration((context1, rxCacheBuilder) -> {//这里可以自己自定义配置 RxCache 的参数
                    rxCacheBuilder.useExpiredDataIfLoaderNotAvailable(true);
                    //想自定义 RxCache 的缓存文件夹或者解析方式, 如改成 FastJson, 请 return rxCacheBuilder.persistence(cacheDirectory, new FastJsonSpeaker());
                    //否则请 return null;
                    return null;
                });
    }
}
