package my.jessyan.mvpart.mvp.slice;

import me.jessyan.art.base.BaseAbilitySlice;
import me.jessyan.art.mvp.IView;
import me.jessyan.art.mvp.Message;
import me.jessyan.art.utils.ArtUtils;
import my.jessyan.mvpart.mvp.ResourceTable;
import my.jessyan.mvpart.mvp.presenter.MainPresenter;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public class MainAbilitySlice extends BaseAbilitySlice<MainPresenter> implements IView {

    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public int initView(@Nullable Intent savedInstanceState) {
        return ResourceTable.Layout_ability_main;
    }

    @Override
    public void initData(@Nullable Intent savedInstanceState) {
        mPresenter.initRefresh();
    }

    @Override
    public @Nullable MainPresenter obtainPresenter() {
        return new MainPresenter(this, ArtUtils.obtainAppComponentFromContext(this.getAbility()));
    }

    @Override
    public void showMessage(@NotNull String message) {

    }

    @Override
    public void handleMessage(@NotNull Message message) {

    }
}
