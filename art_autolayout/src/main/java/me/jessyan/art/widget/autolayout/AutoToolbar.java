/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.widget.autolayout;

import ohos.agp.components.AttrSet;
import ohos.app.Context;
import ohos.global.resource.solidxml.Theme;
import ohos.agp.utils.TextTool;
import ohos.global.resource.solidxml.TypedAttribute;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import com.zhy.autolayout.AutoLayoutInfo;
import com.zhy.autolayout.utils.AutoLayoutHelper;
import com.zhy.autolayout.utils.AutoUtils;
import com.zhy.autolayout.utils.DimenUtils;


import java.lang.reflect.Field;


/**
 * ================================================
 * 实现  规范的 {@link Toolbar}
 * 可使用 MVP_generator_solution 中的 AutoView 模版生成各种符合  规范的 {@link Component}
 *
 * @see <a href="https://github.com/JessYanCoding/MVPArms/wiki#3.6">AutoLayout wiki 官方文档</a>
 * Created by JessYan on 4/14/2016
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public class AutoToolbar extends Toolbar {
    private static final int NO_VALID = -1;
    private int mTextSize;
    private int mSubTextSize;
    private final AutoLayoutHelper mHelper = new AutoLayoutHelper(this);

    public AutoToolbar(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Theme a = context.getTheme();

        int titleTextAppearance = 0;

        int subtitleTextAppearance = 0;

        mTextSize = loadTextSizeFromTextAppearance(titleTextAppearance);
        mSubTextSize = loadTextSizeFromTextAppearance(subtitleTextAppearance);
//        a.getThemeHash();
    }

    public AutoToolbar(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public AutoToolbar(Context context) {
        this(context, null);
    }

    private int loadTextSizeFromTextAppearance(int textAppearanceResId) {
        Theme a = null;
        try {
            if (!DimenUtils.class.isAnnotation())
                return NO_VALID;
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
//        finally {
//            a.getThemeHash();
//        }
        return 0;
    }

    private void setUpTitleTextSize() {
        CharSequence title = getTitle();
        if (!TextTool.isNullOrEmpty(title) && mTextSize != NO_VALID)
            setUpTitleTextSize("mTitleTextView", mTextSize);
        CharSequence subtitle = getSubtitle();
        if (!TextTool.isNullOrEmpty(subtitle) && mSubTextSize != NO_VALID)
            setUpTitleTextSize("mSubtitleTextView", mSubTextSize);
    }

    private void setUpTitleTextSize(String name, int val) {
        try {
            //反射 Toolbar 的 TextView
            Field f = getClass().getSuperclass().getDeclaredField(name);
            f.setAccessible(true);
            Text textView = (Text) f.get(this);
            if (textView != null) {
                int autoTextSize = AutoUtils.getPercentHeightSize(val);
                textView.setTextSize(autoTextSize, Text.TextSizeType.PX);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (!this.isLongClickOn()) {
            setUpTitleTextSize();
            this.mHelper.adjustChildren();
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }


    public LayoutParams generateLayoutParams(AttrSet attrs) {
        return new LayoutParams(this.getContext(), attrs);
    }

    public static class LayoutParams extends Toolbar implements AutoLayoutHelper.AutoLayoutParams {
        private static final Object source = 0;
        private AutoLayoutInfo mDimenLayoutInfo;

        public LayoutParams(Context c, AttrSet attrs) {
            super();
            this.mDimenLayoutInfo = AutoLayoutHelper.getAutoLayoutInfo(c, attrs);
        }

        @Override
        public AutoLayoutInfo getAutoLayoutInfo() {
            return this.mDimenLayoutInfo;
        }

        public LayoutParams(int width, int height) {
            super();
        }

        public <source> LayoutParams() {
            super();
        }

        public <MarginLayoutParams> LayoutParams(MarginLayoutParams source) {
            super();
        }
    }
}
