/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.widget.autolayout;

import com.zhy.autolayout.AutoLayoutInfo;
import com.zhy.autolayout.utils.AutoLayoutHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ScrollView;
import ohos.app.Context;

/**
 * ================================================
 * @see <a href="https://github.com/JessYanCoding/MVPArms/wiki#3.6">AutoLayout wiki 官方文档</a>
 * Created by JessYan on 4/14/2016
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */

public class AutoScrollView extends ScrollView {
    private AutoLayoutHelper mHelper = new AutoLayoutHelper(this);

    public AutoScrollView(Context context) {
        super(context);
    }

    public AutoScrollView(Context context, AttrSet  attrs) {
        super(context, attrs);
    }

    public AutoScrollView(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
    }

    @Override
    public void setComponentSize(int widthMeasureSpec, int heightMeasureSpec) {
        mHelper.adjustChildren();
        super.setComponentSize(widthMeasureSpec, heightMeasureSpec);
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {}

    public LayoutParams generateLayoutParams(AttrSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    public static class LayoutParams extends ScrollView.LayoutConfig
            implements AutoLayoutHelper.AutoLayoutParams {
        private AutoLayoutInfo mAutoLayoutInfo;

        public LayoutParams(Context c, AttrSet attrs) {
            super(c, attrs);
            mAutoLayoutInfo = AutoLayoutHelper.getAutoLayoutInfo(c, attrs);
        }

        @Override
        public AutoLayoutInfo getAutoLayoutInfo() {
            return mAutoLayoutInfo;
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(ComponentContainer.LayoutConfig  source) {
            super(source);
        }

        public LayoutParams(LayoutConfig  source) {
            super(source);
        }
    }
}

