/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.widget.autolayout;

import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Toolbar extends ComponentContainer {
    private static final String TAG = "CustomTitleBar";
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.DEBUG, 0, "TAG");
    private CharSequence mTitleText;
    private CharSequence mSubtitleText;

    public Toolbar(Context context) {
        super(context);
    }

    public Toolbar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        //动态加载layout
        Component component = LayoutScatter.getInstance(context).parse(0, null, false);
        Button leftBtn = (Button) component.findComponentById(0);
        Text titleText = (Text) component.findComponentById(0);
        Button rightBtn = (Button) component.findComponentById(0);
        //添加layout到父组件
        addComponent(component);
        //处理TitleBar背景色
        if (attrSet.getAttr("bg_color").isPresent()) {
            component.setBackground(attrSet.getAttr("bg_color").get().getElement());
        } else {
            HiLog.error(LABEL, "attr bg_color is not present");
            component.setBackground(getBackgroundElement());
        }

        //处理标题文字
        if (attrSet.getAttr("title_text").isPresent()) {
            titleText.setText(attrSet.getAttr("title_text").get().getStringValue());
        } else {
            HiLog.error(LABEL, "attr title_text is not present");
            titleText.setText("");
        }

        //处理标题大小
        if (attrSet.getAttr("title_size").isPresent()) {
            titleText.setTextSize(attrSet.getAttr("title_size").get().getIntegerValue(), Text.TextSizeType.FP);
        } else {
            HiLog.error(LABEL, "attr title_size is not present");
        }
        //处理标题颜色
        if (attrSet.getAttr("title_color").isPresent()) {
            titleText.setTextColor(attrSet.getAttr("title_color").get().getColorValue());
        } else {
            HiLog.error(LABEL, "attr title_color is not exist");
            titleText.setTextColor(Color.BLACK);
        }

        //处理左边按钮
        leftButton(attrSet, leftBtn);

        //处理右边按钮
        rightButton(attrSet, rightBtn);
    }

    public Toolbar() {
        super(null);
    }

   public void generateLayoutParams(){
   }
   
    public CharSequence getTitle() {
        return this.mTitleText;
    }
    public CharSequence getSubtitle() {
        return this.mSubtitleText;
    }
    /**
     * 处理左边按钮
     * @param attrSet
     * @param leftBtn
     */
    private void leftButton(AttrSet attrSet, Button leftBtn) {
        //获取是否要显示左边按钮
        if (attrSet.getAttr("left_button_visible").isPresent()) {
            if (attrSet.getAttr("left_button_visible").get().getBoolValue()) {
                leftBtn.setVisibility(VISIBLE);
            } else {
                leftBtn.setVisibility(INVISIBLE);
            }
        } else {
            //默认情况显示
            HiLog.error(LABEL, "attr right_button_visible is not exist");
            leftBtn.setVisibility(VISIBLE);
        }
        //处理左侧按钮的图标
        if (attrSet.getAttr("left_button_icon").isPresent()) {
            leftBtn.setAroundElements(attrSet.getAttr("left_button_icon").get().getElement(), null, null, null);
        } else {
            HiLog.error(LABEL, "attr left_button_icon is not exist");
        }
        //处理左侧按钮的文本
        if (attrSet.getAttr("left_button_text").isPresent()) {
            leftBtn.setText(attrSet.getAttr("left_button_text").get().getStringValue());
        } else {
            HiLog.error(LABEL, "attr left_button_text is not exist");
        }
        //处理左侧按钮的文本颜色
        if (attrSet.getAttr("left_button_text_color").isPresent()) {
            leftBtn.setTextColor(attrSet.getAttr("left_button_text_color").get().getColorValue());
        } else {
            HiLog.error(LABEL, "attr left_button_text_color is not exist");
        }
        //处理左侧按钮的文本大小
        if (attrSet.getAttr("left_button_text_size").isPresent()) {
            leftBtn.setTextSize(attrSet.getAttr("left_button_text_size").get().getIntegerValue(), Text.TextSizeType.FP);
        } else {
            HiLog.error(LABEL, "attr left_button_text_size is not exist");
        }
    }

    /**
     * 处理右边按钮
     * @param attrSet
     * @param rightBtn
     */
    private void rightButton(AttrSet attrSet, Button rightBtn) {
        //获取是否要显示右边按钮
        if (attrSet.getAttr("right_button_visible").isPresent()) {
            if (attrSet.getAttr("right_button_visible").get().getBoolValue()) {
                rightBtn.setVisibility(VISIBLE);
            } else {
                rightBtn.setVisibility(INVISIBLE);
            }
        } else {
            //默认情况显示
            HiLog.error(LABEL, "attr right_button_visible is not exist");
            rightBtn.setVisibility(VISIBLE);
        }

        //处理右侧按钮的图标
        if (attrSet.getAttr("right_button_icon").isPresent()) {
            rightBtn.setAroundElements(attrSet.getAttr("right_button_icon").get().getElement(), null, null, null);
        } else {
            HiLog.error(LABEL, "attr right_button_icon is not exist");
        }
        //处理右侧按钮的文本
        if (attrSet.getAttr("right_button_text").isPresent()) {
            rightBtn.setText(attrSet.getAttr("right_button_text").get().getStringValue());
        } else {
            HiLog.error(LABEL, "attr right_button_text is not exist");
        }
        //处理右侧按钮的文本颜色
        if (attrSet.getAttr("right_button_text_color").isPresent()) {
            rightBtn.setTextColor(attrSet.getAttr("right_button_text_color").get().getColorValue());
        } else {
            HiLog.error(LABEL, "attr right_button_text_color is not exist");
        }
        //处理右侧按钮的文本大小
        if (attrSet.getAttr("right_button_text_size").isPresent()) {
            rightBtn.setTextSize(attrSet.getAttr("right_button_text_size").get().getIntegerValue(), Text.TextSizeType.FP);
        } else {
            HiLog.error(LABEL, "attr right_button_text_size is not exist");
        }
    }

    public Toolbar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    }
}
