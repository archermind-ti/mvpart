/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.widget.autolayout;

import com.zhy.autolayout.utils.AutoLayoutHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;

import ohos.app.Context;

/**
 * ================================================
 * @see <a href="https://github.com/JessYanCoding/MVPArms/wiki#3.6">AutoLayout wiki 官方文档</a>
 * Created by JessYan on 9/3/16 21:40
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public class AutoCardView extends DependentLayout {
    private AutoLayoutHelper mHelper = new AutoLayoutHelper(this);

    public AutoCardView(Context context) {
        super(context);
    }

    public AutoCardView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public AutoCardView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    public ComponentContainer.LayoutConfig generateLayoutParams(AttrSet attrs) {
        return new ComponentContainer.LayoutConfig(getContext(),attrs);
    }
    public void setComponentSize(int widthMeasureSpec, int heightMeasureSpec) {
        mHelper.adjustChildren();
        super.setComponentSize(widthMeasureSpec, heightMeasureSpec);
    }

}