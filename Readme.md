# MVPArt

## A New MVP Architecture
**此框架旨在解决传统 **MVP** 类和接口太多,并且 **Presenter** 和 **View** 通过接口通信过于繁琐,重用 **Presenter** 代价太大等问题**

## Introduction
含有网络层的完整框架,将 **Retrofit** 作为网络层并使用 **Dagger2** 管理所有对象,成熟强大适合新建的项目

## Notice
* 此框架是一个轻量级框架,比较适合中小型项目
* 传统MVP每个页面对应一个 **Presenter** ,而大多数 **Presenter** 只有一两个方法,这样导致存在大量代码寥寥无几的 **Presenter** ,此框架旨在解决复用 **Presenter** 时需要实现过多多余接口方法的问题,鼓励开发者将相近的逻辑写在一个 **Presenter** 中,不断重用 **Presenter** ,减少大量类文件
* 当然很多不同的逻辑都写在一个 **Presenter** 中,虽然可以少写很多类,但是后面的扩展性肯定不好,所以这个粒度需要自己控制,但是对于外包项目简直是福音

## 集成
##### 方法1: 直接使用har包
```
通过library生成har包，添加har包到要集成的libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

##### 方法2: gradle依赖
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation "com.gitee.archermind-ti:art:1.0.0-beta"
implementation "com.gitee.archermind-ti:art-autolayout:1.0.0-beta"
implementation "com.gitee.archermind-ti:art-imageloader-glide:1.0.0-beta"

```
## 使用方法
1. MainAbility.java 继承 BaseApplication
```
public class MainAbility extends BaseApplication {}
```
2. 配置baseUrl
```
public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        configApi();
    }

private void configApi() {
        GlobalConfigModule.Builder builder = GlobalConfigModule.builder();
        builder.baseurl(Api.APP_DOMAIN);
    }
```
3. MainAbilitySlice 继承 BaseAbilitySlice 并实现 IView
```
public class MainAbilitySlice extends BaseAbilitySlice<MainPresenter> implements IView
```

4. 通过 ArtUtils.obtainAppComponentFromContext 获取 AppComponent
```
    public @Nullable MainPresenter obtainPresenter() {
        return new MainPresenter(this, ArtUtils.obtainAppComponentFromContext(this.getAbility()));
    }
```

5. 其他详见代码注释以及sample（entry）中使用方法 

## 遗留问题 
 - 未实现图片网络请求，因为目前不支持使用GlideRequests和GlideArt请求网络图片
 
## License
```
 Copyright 2017, jessyan

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```

