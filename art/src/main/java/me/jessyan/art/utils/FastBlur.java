/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.utils;

import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.agp.render.Canvas;

import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;

import static ohos.media.image.common.PixelFormat.ARGB_8888;

/**
 * ================================================
 * 处理高斯模糊的工具类
 * <p>
 * Created by JessYan on 03/06/2014.
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public final class FastBlur {
    static final int NUM_256 = 256;
    static final int NUM_3 = 3;
    static final int NUM_0x = 0xff0000;
    static final int NUM_0xf = 0xff000000;
    static final int NUM_0f = 0x00ff00;
    static final int NUM_00 = 0x0000ff;
    static final int NUM_15 = 15;
    static final int NUM_16 = 16;
    static final int NUM_8 = 8;


    private FastBlur() {
        throw new IllegalStateException("you can't instantiate me!");
    }

    public static PixelMap doBlur(PixelMap sentBitmap, int radius, boolean canReuseInBitmap) {

        // Stack Blur v1.0 from
        // http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
        //
        // Java Author: Mario Klingemann <mario at quasimondo.com>
        // http://incubator.quasimondo.com
        // created Feburary 29, 2004
        // http://www.kayenko.com
        // ported april 5th, 2012

        // This is a compromise between Gaussian Blur and Box blur
        // It creates much better looking blurs than Box Blur, but is
        // 7x faster than my Gaussian Blur implementation.
        //
        // I called it Stack Blur because this describes best how this
        // filter works internally: it creates a kind of moving stack
        // of colors whilst scanning through the image. Thereby it
        // just has to add one new block of color to the right side
        // of the stack and remove the leftmost color. The remaining
        // colors on the topmost layer of the stack are either added on
        // or reduced by one, depending on if they are on the right or
        // on the left side of the stack.
        //
        // If you are using this algorithm in your code please add
        // the following line:
        //
        // Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>

        PixelMap bitmap;
        bitmap = sentBitmap;

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getImageInfo().size.width;
        int h = bitmap.getImageInfo().size.height;

        int[] pix = new int[w * h];
        bitmap.readPixels(pix, w, h, null);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[NUM_256 * divsum];
        for (i = 0; i < NUM_256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = 0;
        yi = 0;

        int[][] stack = new int[div][NUM_3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = 0;
            ginsum = 0;
            binsum = 0;
            routsum = 0;
            goutsum = 0;
            boutsum = 0;
            rsum = 0;
            gsum = 0;
            bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & NUM_0x) >> NUM_16;
                sir[1] = (p & NUM_0f) >> NUM_8;
                sir[2] = (p & NUM_00);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & NUM_0x) >> NUM_16;
                sir[1] = (p & NUM_0f) >> NUM_8;
                sir[2] = (p & NUM_00);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = 0;
            ginsum = 0;
            binsum = 0;
            routsum = 0;
            goutsum = 0;
            boutsum = 0;
            rsum = 0;
            gsum = 0;
            bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (NUM_0xf & pix[yi]) | (dv[rsum] << NUM_16) | (dv[gsum] << NUM_8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.readPixels(pix, w, h, null);

        return (bitmap);
    }

    /**
     * 给 {@link Component} 设置高斯模糊背景图片
     *
     * @param context
     * @param bkg
     * @param view
     */
    public static void blur(Context context, PixelMapHolder bkg, Component view, PixelMap pix) {
        long startMs = System.currentTimeMillis();
        float radius = NUM_15;
        float scaleFactor = NUM_8;
        //放大到整个view的大小

    /*    PixelMap overlay = PixelMap.create((int) (view.getEstimatedWidth() / scaleFactor)
                , (int) (view.getEstimatedHeight() / scaleFactor), ARGB_8888);*/
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size.width = (int) (view.getEstimatedWidth() / scaleFactor);
        options.size.height = (int) (view.getEstimatedHeight() / scaleFactor);
        options.pixelFormat = ARGB_8888;
        PixelMap overlay = PixelMap.create(options);
        Texture texture = new Texture(overlay);
        Canvas canvas = new Canvas(texture);
        canvas.translate(-view.getLeft() / scaleFactor, -view.getTop() / scaleFactor);
        canvas.scale(1 / scaleFactor, 1 / scaleFactor);
        Paint paint = new Paint();
        paint.getPosition().equals(2);
        canvas.drawPixelMapHolder(bkg, 0, 0, paint);
        //view.setBackgroundDrawable(new BitmapDrawable(context.getResourceManager(), overlay));
        HiLog.warn(null, "cost " + (System.currentTimeMillis() - startMs) + "ms");
    }

    /**
     * 将 {@link PixelMap} 高斯模糊并返回
     *
     * @param bkg
     * @param width
     * @param height
     * @return
     */
    //

//    public static PixelMap blurBitmap(PixelMap bkg, int width, int height) {
//        long startMs = System.currentTimeMillis();
//        float radius = 15;//越大模糊效果越大
//        float scaleFactor = 8;
//        //放大到整个view的大小
//        bkg = DrawableProvider.getReSizeBitmap(bkg, width, height);
//
//
////        PixelMap overlay = PixelMap.create((int) (width / scaleFactor)
////                , (int) (height / scaleFactor), PixelMap.Config.ARGB_8888);
//
//        PixelMap.InitializationOptions options=new PixelMap.InitializationOptions();
//        options.size.width=(int)(view.getEstimatedWidth() / scaleFactor);
//        options.size.height=(int) (view.getEstimatedHeight() / scaleFactor);
//        options.pixelFormat=ARGB_8888;
//        PixelMap overlay = PixelMap.create(options);
//        Texture texture=new Texture(overlay);
//
//        Canvas canvas = new Canvas(texture);
//        canvas.scale(1 / scaleFactor, 1 / scaleFactor);
//        Paint paint = new Paint();
//        paint.getPosition().equals(2);
//        canvas.drawPixelMapHolderRect(bkg, 0, 0, paint);
//        overlay = FastBlur.doBlur(overlay, (int) radius, true);
//        HiLog.warn(null, "cost " + (System.currentTimeMillis() - startMs) + "ms");
//        return overlay;
//    }
}
