/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.utils;

import ohos.agp.utils.Rect;
import ohos.media.image.PixelMap;
import ohos.agp.utils.Matrix;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.StateElement;

import ohos.agp.components.Text;


/**
 * ================================================
 * 处理 {@link Element} 和 {@link PixelMap} 的工具类
 * <p>
 * Created by JessYan on 2015/11/24
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public final class DrawableProvider {
    static final float NUM_0 = 0.5f;

    private DrawableProvider() {
        throw new IllegalStateException("you can't instantiate me!");
    }

    /**
     * 获得选择器
     *
     * @param normalDrawable
     * @param pressDrawable
     * @return
     */
    public static Element getStateListDrawable(Element normalDrawable, Element pressDrawable) {
        StateElement stateElement = new StateElement();
        stateElement.addState(new int[]{}, pressDrawable);
        stateElement.addState(new int[]{}, normalDrawable);
        return stateElement;
    }

    /**
     * 将 TextView/RadioButton 中设置的 drawable 动态的缩放
     *
     * @param percent
     * @param tv
     * @return
     */
    public static Element getScaleDrawableForRadioButton(float percent, Text tv) {
        Element[] compoundDrawables = tv.getAroundElements();
        Element element = null;
        for (Element d : compoundDrawables) {
            if (d != null) {
                element = d;
            }
        }
        return getScaleDrawable(percent, element);
    }

    /**
     * 将 TextView/RadioButton 中设置的 drawable 动态的缩放
     *
     * @param tv
     * @return
     */
    public static Element getScaleDrawableForRadioButton2(float width, Text tv) {
        Element[] compoundDrawables = tv.getAroundElements();
        Element element = null;
        for (Element d : compoundDrawables) {
            if (d != null) {
                element = d;
            }
        }
        return getScaleDrawable2(width, element);
    }

    /**
     * 传入图片,将图片按传入比例缩放
     *
     * @param percent
     * @return
     */
    public static Element getScaleDrawable(double percent, Element element) {
        Rect rect = new Rect(0, 0, (int) (element.getWidth() * percent + NUM_0), (int) (element.getHeight() * percent + NUM_0));
        element.setBounds(rect);
        return element;
    }

    /**
     * 传入图片,将图片按传入宽度和原始宽度的比例缩放
     *
     * @param width
     * @return
     */
    public static Element getScaleDrawable2(float width, Element element) {
        float percent = width * 1.0f / element.getWidth();
        return getScaleDrawable(percent, element);
    }

    /**
     * 设置左边的drawable
     *
     * @param tv
     * @param element
     */
    public static void setLeftDrawable(Text tv, Element element) {
        tv.setAroundElements(element, null, null, null);
    }

    /**
     * 改变Bitmap的长宽
     *
     * @param pixelMap
     * @return
     */


    public static PixelMap getReSizeBitmap(PixelMap pixelMap, float targetWidth, float targetheight) {
        PixelMap returnBm = null;
        int width = pixelMap.getImageInfo().size.width;
        int height = pixelMap.getImageInfo().size.height;
        Matrix matrix = new Matrix();
        matrix.postScale(targetWidth / width, targetheight / height); //长和宽放大缩小的比例
        try {
            // returnBm = PixelMap.readPixels(pixelMap, width, height, null);
            returnBm = PixelMap.create(pixelMap, new ohos.media.image.common.Rect(0, 0, width, height), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (returnBm == null) {
            returnBm = pixelMap;
        }
        if (pixelMap != returnBm) {
            pixelMap.release();
        }
        return returnBm;
    }

    /**
     * 将图片按照某个角度进行旋转
     *
     * @param bm     需要旋转的图片
     * @param degree 旋转角度
     * @return 旋转后的图片
     */
}
