/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.utils;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;


import static me.jessyan.art.base.Platform.DEPENDENCY_AUTO_LAYOUT;


/**
 * ================================================
 * Created by JessYan on 17/03/2016 13:59
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public final class ThirdViewUtil {
    private static int HAS_AUTO_LAYOUT_META = -1;//0  里面没有使用 AutoLauout 的 Meta, 即不使用 AutoLayout, 1 为有 Meta, 即需要使用

    private ThirdViewUtil() {
        throw new IllegalStateException("you can't instantiate me!");
    }

    public static boolean isUseAutolayout() {
        return DEPENDENCY_AUTO_LAYOUT && HAS_AUTO_LAYOUT_META == 1;
    }

    public static Unbinder bindTarget(Object target, Object source) {
        if (source instanceof AbilitySlice) {
            return ButterKnife.bind(target, ((AbilitySlice) source).getAbility());
        } else if (source instanceof Component) {
            return ButterKnife.bind(target, (Component) source);
        } else if (source instanceof ToastDialog) {
            return ButterKnife.bind(target, (ToastDialog) source);
        } else {
            return Unbinder.EMPTY;
        }
    }

//    TODO AutoLayout 未提供
//    @Nullable
//    public static Component convertAutoView(String name, Context context, AttributeSet attrs) {
//        //本框架并不强制您使用 AutoLayout
//        if (!DEPENDENCY_AUTO_LAYOUT) return null;
//        if (HAS_AUTO_LAYOUT_META == -1) {
//            HAS_AUTO_LAYOUT_META = 1;
//            PackageManager packageManager = context.getPackageManager();
//            ApplicationInfo applicationInfo;
//            try {
//                applicationInfo = packageManager.getApplicationInfo(context
//                        .getPackageName(), PackageManager.GET_META_DATA);
//                if (applicationInfo == null || applicationInfo.metaData == null
//                        || !applicationInfo.metaData.containsKey("design_width")
//                        || !applicationInfo.metaData.containsKey("design_height")) {
//                    HAS_AUTO_LAYOUT_META = 0;
//                }
//            } catch (PackageManager.NameNotFoundException e) {
//                HAS_AUTO_LAYOUT_META = 0;
//            }
//        }
//
//        if (HAS_AUTO_LAYOUT_META == 0) {
//            return null;
//        }
//
//        View view = null;
//        if (name.equals(LAYOUT_STACK)) {
//            view = new AutoFrameLayout(context, attrs);
//        } else if (name.equals(LAYOUT_DIRECTIONAL)) {
//            view = new AutoLinearLayout(context, attrs);
//        } else if (name.equals(LAYOUT_DEPENDENT)) {
//            view = new AutoRelativeLayout(context, attrs);
//        }
//        return view;
//    }
}

