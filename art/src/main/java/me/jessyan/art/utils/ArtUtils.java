/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.utils;

import java.io.IOException;
import me.jessyan.art.base.App;
import me.jessyan.art.di.component.AppComponent;
import me.jessyan.art.integration.AppManager;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.global.resource.Element;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

/**
 * ================================================
 * 一些框架常用的工具
 * <p>
 * Created by JessYan on 2015/11/23.
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public final class ArtUtils {
    public static  ToastDialog mToast;

    private ArtUtils() {
        throw new IllegalStateException("you can't instantiate me!");
    }

    /**
     * 获得资源
     */
    public static ResourceManager getResources(Context context) {
        return context.getResourceManager();
    }

    /**
     * 得到字符数组
     */
    public static String[] getStringArray(Context context, int id) {
        return context.getStringArray(id);
    }

    /**
     * 从 dimens 中获得尺寸
     *
     * @param context
     * @param id
     * @return
     */
    public static int getDimens(Context context, int id) throws NotExistException, WrongTypeException, IOException {
        return (int) getResources(context).getElement(id).getFloat();
    }

    /**
     * 从String 中获得字符
     *
     * @return
     */
    public static String getString(Context context, int stringID) throws NotExistException, WrongTypeException, IOException {
        return getResources(context).getElement(stringID).getString();
    }

    /**
     * 单例 toast
     *
     * @param string
     */
    public static void makeText(Context context, String string) {
       new ToastDialog(context)
                .setText(string)
                .setAlignment(LayoutAlignment.CENTER)
                .show();
    }


    /**
     * 通过资源id获得drawable
     *
     * @param
     * @return
     */
    public static Element getDrawablebyResource(Context context, int rID) throws NotExistException, WrongTypeException, IOException {
        return getResources(context).getElement(rID);
    }


    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        }
        return false;
    }
    
    /**
     * 执行 {@link AppManager#killAll()}
     */
    public static void killAll() {
        AppManager.getAppManager().killAll();
    }

    /**
     * 执行 {@link AppManager#appExit()}
     */
    public static void exitApp() {
        AppManager.getAppManager().appExit();
    }

    public static AppComponent obtainAppComponentFromContext(Context context) {
        Preconditions.checkNotNull(context, "%s cannot be null", Context.class.getName());
        Preconditions.checkState(context instanceof App, "%s must be implements %s", context.getApplicationContext().getClass().getName(), App.class.getName());
        AppComponent appComponent = ((App) context).getAppComponent();
        return appComponent;
    }
}
