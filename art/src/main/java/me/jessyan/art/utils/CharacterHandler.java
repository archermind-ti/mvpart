/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.utils;


import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONException;
import ohos.utils.zson.ZSONObject;



/**
 * ================================================
 * 处理字符串的工具类
 * <p>
 * Created by JessYan on 2016/3/16
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public final class CharacterHandler {
    static final int NUM_0= 0x0f0;
    static final int NUM_4=4;
    static final int NUM_01= 0x0f;

    private CharacterHandler() {
        throw new IllegalStateException("you can't instantiate me!");
    }

//    TODO InputFilter 未提供

//    public static final InputFilter emojiFilter = new InputFilter() {//emoji过滤器
//        Pattern emoji = Pattern.compile(
//                "[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
//                Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
//
//        @Override
//        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart,
//                                   int dend) {
//
//            Matcher emojiMatcher = emoji.matcher(source);
//            if (emojiMatcher.find()) {
//                return "";
//            }
//
//            return null;
//        }
//    };

    /**
     * 字符串转换成十六进制字符串
     *
     * @return String 每个Byte之间空格分隔，如: [61 6C 6B]
     */
    public static String str2HexStr(String str) {

        char[] chars = "0123456789ABCDEF".toCharArray();
        StringBuilder sb = new StringBuilder("");
        byte[] bs = str.getBytes();
        int bit;

        for (int i = 0; i < bs.length; i++) {
            bit = (bs[i] & NUM_0) >> NUM_4;
            sb.append(chars[bit]);
            bit = bs[i] & NUM_01;
            sb.append(chars[bit]);
        }
        return sb.toString().trim();
    }

    /**
     * json 格式化
     *
     * @param json
     * @return
     */
    public static String jsonFormat(String json) {
        if (null == json || json.length() == 0) {
            return "Empty/Null json content";
        }
        String message;
        try {
            json = json.trim();
            if (json.startsWith("{")) {
                ZSONObject jsonObject = new ZSONObject();
                jsonObject.stringToZSON(json);
                message = jsonObject.toString();
            } else if (json.startsWith("[")) {
                ZSONArray jsonArray = new ZSONArray();
                jsonArray.stringToZSONArray(json);
                message = jsonArray.toString();
            } else {
                message = json;
            }
        } catch (ZSONException e) {
            message = json;
        }
        return message;
    }
}
