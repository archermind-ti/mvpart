/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.base;


import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.agp.components.LayoutScatter;

import ohos.agp.components.ComponentContainer;
import me.jessyan.art.base.delegate.IFraction;
import me.jessyan.art.integration.cache.Cache;
import me.jessyan.art.integration.cache.CacheType;
import me.jessyan.art.mvp.IPresenter;
import me.jessyan.art.utils.ArtUtils;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.utils.PacMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * ================================================
 * 因为 Java 只能单继承, 所以如果要用到需要继承特定 @{@link Fraction} 的三方库, 那你就需要自己自定义 @{@link Fraction}
 * 继承于这个特定的 @{@link Fraction}, 然后再按照 {@link BaseFraction} 的格式, 将代码复制过去, 记住一定要实现{@link IFraction}
 *
 * @see <a href="https://github.com/JessYanCoding/MVPArms/wiki">请配合官方 Wiki 文档学习本框架 (Arms 的文档除了 MVP 部分, 其他的文档内容 Art 和 Arms 都可以共用)</a>
 * @see <a href="https://github.com/JessYanCoding/MVPArms/wiki/Issues">常见 Issues, 踩坑必看!</a>
 * Created by JessYan on 22/03/2016
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public abstract class BaseFraction<P extends IPresenter> extends FractionAbility implements IFraction<P> {
    protected final String TAG = this.getClass().getSimpleName();
    private Cache<String, Object> mCache;
    protected Context mContext;
    protected P mPresenter;

    @NotNull
    @Override
    public synchronized Cache<String, Object> provideCache() {
        if (mCache == null) {
            mCache = ArtUtils.obtainAppComponentFromContext(this).cacheFactory().build(CacheType.FRAGMENT_CACHE);
        }
        return mCache;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Nullable
    public Component onComponentDetach(@NotNull LayoutScatter inflater, @Nullable ComponentContainer container, @Nullable PacMap savedInstanceState) {
        return initView(inflater, container, savedInstanceState);
    }

    @Override
    public void setPresenter(@Nullable P presenter) {
        this.mPresenter = presenter;
    }

     @Override
     public void onRestoreAbilityState(@Nullable PacMap savedInstanceState) {
         super.onRestoreAbilityState(savedInstanceState);
         if (mPresenter == null) {
             mPresenter = obtainPresenter();
         }
     }

    @Override
    public void onStop() {
        super.onStop();
        mContext = null;
    }

    /**
     * 是否使用 EventBus
     * Art 核心库现在并不会依赖某个 EventBus, 要想使用 EventBus, 还请在项目中自行依赖对应的 EventBus
     * 现在支持两种 EventBus, greenrobot 的 EventBus
     * 确保依赖后, 将此方法返回 true, Art 会自动检测您依赖的 EventBus, 并自动注册
     * 这种做法可以让使用者有自行选择三方库的权利, 并且还可以减轻 Art 的体积
     *
     * @return 返回 {@code true} (默认为 {@code true}), Art 会自动注册 EventBus
     */
    @Override
    public boolean useEventBus() {
        return true;
    }
}
