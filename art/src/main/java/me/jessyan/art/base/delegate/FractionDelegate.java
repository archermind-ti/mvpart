/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.base.delegate;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;

import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

import ohos.utils.PacMap;



/**
 * ================================================
 * {@link Fraction} 代理类,用于框架内部在每个 {@link Fraction} 的对应生命周期中插入需要的逻辑
 *
 * @see FractionDelegateImpl
 * @see <a href="https://github.com/JessYanCoding/MVPArms/wiki#3.13">FractionDelegate wiki 官方文档</a>
 * Created by JessYan on 29/04/2017 14:30
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public interface FractionDelegate {
    String FRACTION_DELEGATE = "FRACTION_DELEGATE";

    void onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent);

    void onStart(Intent intent);

    void onActive();

    void onInactive();

    void onForeground(Intent intent);

    void onBackground();

    void onStop();

    void onComponentDetach();

    void onPostStart(PacMap pacMap);

    void onSaveAbilityState(PacMap outState);

    void onRestoreAbilityState(PacMap inState);

    void onBackPressed();

    /**
     * Return true if the fragment is currently added to its activity.
     */
    boolean isAdded();
}
