/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.base.delegate;

import me.jessyan.art.integration.EventBusManager;
import me.jessyan.art.mvp.IPresenter;
import ohos.aafwk.ability.Ability;

import ohos.aafwk.ability.LifecycleObserver;
import ohos.aafwk.content.Intent;
import ohos.utils.PacMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * ================================================
 * {@link AbilityDelegate} 默认实现类
 * <p>
 * Created by JessYan on 26/04/2017 20:23
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public class AbilityDelegateImpl implements AbilityDelegate {
    private Ability mAbility;
    private IAbility iAbility;
    private IPresenter iPresenter;

    public AbilityDelegateImpl(@NotNull Ability ability) {
        this.mAbility = ability;
        this.iAbility = (IAbility) ability;
    }

    @Override
    public void onCreate(@Nullable PacMap savedInstanceState) {

    }

    @Override
    public void onStart(@Nullable Intent intent) {
        //如果要使用 EventBus 请将此方法返回 true
        if (iAbility.useEventBus()) {
            //注册到事件主线
            EventBusManager.getInstance().register(mAbility);
        }
        this.iPresenter = iAbility.obtainPresenter();
        iAbility.setPresenter(iPresenter);
        //将 LifecycleObserver 注册给 LifecycleOwner 后 @OnLifecycleEvent 才可以正常使用
        if (mAbility != null && iPresenter != null && iPresenter instanceof LifecycleObserver) {
            mAbility.getLifecycle().addObserver((LifecycleObserver) iPresenter);
        }
    }

    @Override
    public void onActive() {

    }

    @Override
    public void onInactive() {

    }

    @Override
    public void onBackground() {

    }

    @Override
    public void onForeground(Intent intent) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {
        //如果要使用 EventBus 请将此方法返回 true
        if (iAbility != null && iAbility.useEventBus())
            EventBusManager.getInstance().unregister(mAbility);
        //释放资源
        if (iPresenter != null) iPresenter.onDestroy();
        this.iAbility = null;
        this.mAbility = null;
        this.iPresenter = null;
    }

    @Override
    public void onSaveInstanceState(@NotNull PacMap outState) {

    }

    @Override
    public void onSaveAbilityState(@NotNull PacMap outState) {

    }
}
