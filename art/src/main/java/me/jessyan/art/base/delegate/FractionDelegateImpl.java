/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.base.delegate;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ohos.aafwk.ability.LifecycleObserver;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.utils.PacMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import me.jessyan.art.integration.EventBusManager;
import me.jessyan.art.mvp.IPresenter;

/**
 * ================================================
 * {@link FractionDelegate} 默认实现类
 * <p>
 * Created by JessYan on 29/04/2017 16:12
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public class FractionDelegateImpl implements FractionDelegate {
    private ohos.aafwk.ability.fraction.FractionManager mFractionManager;
    private ohos.aafwk.ability.fraction.Fraction mFraction;
    private IFraction iFraction;
    private Unbinder mUnbinder;
    private IPresenter iPresenter;

    public FractionDelegateImpl(@NotNull ohos.aafwk.ability.fraction.FractionManager fractionManager, @NotNull ohos.aafwk.ability.fraction.Fraction fraction) {
        this.mFractionManager = fractionManager;
        this.mFraction = fraction;
        this.iFraction = (IFraction) fraction;
    }


    @Override
    public void onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {

    }

    public void onCreateView(@Nullable Component view, @Nullable Intent savedInstanceState) {
        //绑定到butterknife
        if (view != null)
            mUnbinder = ButterKnife.bind(mFraction, view);
    }

    @Override
    public void onStart(Intent intent) {
        if (iFraction.useEventBus())//如果要使用eventbus请将此方法返回true
            EventBusManager.getInstance().register(mFraction);//注册到事件主线
        this.iPresenter = iFraction.obtainPresenter();
        iFraction.setPresenter(iPresenter);
        //将 LifecycleObserver 注册给 LifecycleOwner 后 @OnLifecycleEvent 才可以正常使用
        if (mFraction != null && iPresenter != null && iPresenter instanceof LifecycleObserver){
            mFraction.getLifecycle().addObserver((LifecycleObserver) iPresenter);
        }
    }

    @Override
    public void onActive() {

    }

    @Override
    public void onInactive() {

    }

    @Override
    public void onForeground(Intent intent) {

    }

    @Override
    public void onBackground() {

    }

    @Override
    public void onStop() {
        if (iFraction != null && iFraction.useEventBus())//如果要使用eventbus请将此方法返回true
            EventBusManager.getInstance().unregister(mFraction);//注册到事件主线
        if (iPresenter != null) iPresenter.onDestroy(); //释放资源
        this.mFractionManager = null;
        this.mFraction = null;
        this.iFraction = null;
        this.iPresenter = null;
    }

    @Override
    public void onComponentDetach() {

    }

    @Override
    public void onPostStart(PacMap pacMap) {

    }

    @Override
    public void onSaveAbilityState(PacMap outState) {

    }

    @Override
    public void onRestoreAbilityState(PacMap inState) {

    }

    @Override
    public void onBackPressed() {

    }

    /**
     * Return true if the fragment is currently added to its activity.
     */
    @Override
    public boolean isAdded() {
        return mFraction != null ;
    }
}
