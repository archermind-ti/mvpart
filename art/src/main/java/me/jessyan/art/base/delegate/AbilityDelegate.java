/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.base.delegate;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.utils.PacMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * ================================================
 * {@link AbilitySlice} 代理类,用于框架内部在每个 {@link ohos.aafwk.ability.Ability} 的对应生命周期中插入需要的逻辑
 *
 * @see AbilityDelegateImpl
 * @see <a href="https://github.com/JessYanCoding/MVPArms/wiki#3.13">AbilityDelegate wiki 官方文档</a>
 * Created by JessYan on 26/04/2017 20:23
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public interface AbilityDelegate {
    String LAYOUT_DIRECTIONAL = "DirectionalLayout";
    String LAYOUT_STACK = "StackLayout";
    String LAYOUT_DEPENDENT = "DependentLayout";
    String ACTIVITY_DELEGATE = "ACTIVITY_DELEGATE";

    void onCreate(@Nullable PacMap savedInstanceState);

    void onStart(@Nullable Intent intent);

    void onActive();

    void onInactive();

    void onBackground();

    void onForeground(Intent intent);

    void onStop();

    void onResume();

    void onPause();

    void onSaveInstanceState(@NotNull PacMap outState);

    void onSaveAbilityState(@NotNull PacMap outState);
}
