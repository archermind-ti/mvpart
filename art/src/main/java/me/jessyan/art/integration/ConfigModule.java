/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.integration;

import ohos.aafwk.ability.Ability;

import ohos.app.Context;
import org.jetbrains.annotations.NotNull;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.AbilityLifecycleCallbacks;
import me.jessyan.art.base.delegate.AppLifecycles;
import me.jessyan.art.di.module.GlobalConfigModule;

import java.util.List;

/**
 * ================================================
 * {@link ConfigModule} 可以给框架配置一些参数,需要实现 {@link ConfigModule}
 *
 * @see <a href="https://github.com/JessYanCoding/MVPArms/wiki#2.1">ConfigModule wiki 官方文档</a>
 * Created by JessYan on 12/04/2017 11:37
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public interface ConfigModule {

    /**
     * 使用 {@link GlobalConfigModule.Builder} 给框架配置一些配置参数
     *
     * @param context {@link Context}
     * @param builder {@link GlobalConfigModule.Builder}
     */
    void applyOptions(@NotNull Context context, @NotNull GlobalConfigModule.Builder builder);

    /**
     * 使用 {@link AppLifecycles} 在 {@link Ability} 的生命周期中注入一些操作
     *
     * @param context    {@link Context}
     * @param lifecycles {@link Ability} 的生命周期容器, 可向框架中添加多个 {@link Ability} 的生命周期类
     */
    void injectAppLifecycle(@NotNull Context context, @NotNull List<AppLifecycles> lifecycles);

    /**
     * 使用 {link Application.ActivityLifecycleCallbacks} 在 {@link Ability} 的生命周期中注入一些操作
     *
     * @param context    {@link Context}
     * @param lifecycles {@link Ability} 的生命周期容器, 可向框架中添加多个 {@link Ability} 的生命周期类
     */
    void injectActivityLifecycle(@NotNull Context context, @NotNull List<AbilityLifecycleCallbacks> lifecycles);

    /**
     * 使用 {link FragmentManager.FragmentLifecycleCallbacks} 在 {@link Fraction} 的生命周期中注入一些操作
     *
     * @param context    {@link Context}
     * @param lifecycles {@link Fraction} 的生命周期容器, 可向框架中添加多个 {@link Fraction} 的生命周期类
     */
    void injectFragmentLifecycle(@NotNull Context context, @NotNull List<AbilityLifecycleCallbacks> lifecycles);
}
