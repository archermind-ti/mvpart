/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.art.integration;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.annotations.NonNull;
import me.jessyan.art.base.delegate.FractionDelegate;
import me.jessyan.art.base.delegate.FractionDelegateImpl;
import me.jessyan.art.base.delegate.IFraction;
import me.jessyan.art.integration.cache.Cache;
import me.jessyan.art.integration.cache.IntelligentCache;
import me.jessyan.art.utils.Preconditions;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilityLifecycleCallbacks;
import ohos.aafwk.ability.fraction.Fraction;

import ohos.aafwk.ability.fraction.FractionManager;

import ohos.app.Context;
import ohos.utils.PacMap;

/**
 * ================================================
 * <p>
 * Created by JessYan on 04/09/2017 16:04
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
@Singleton
public class FractionLifecycle implements AbilityLifecycleCallbacks {

    @Inject
    public FractionLifecycle() {
    }

    public void onFractionAttached(FractionManager fm, Fraction f, Context context) {
        if (f instanceof IFraction) {
            FractionDelegate fractionDelegate = fetchFragmentDelegate(f);
            if (fractionDelegate == null || !fractionDelegate.isAdded()) {
                Cache<String, Object> cache = getCacheFromFragment((IFraction) f);
                fractionDelegate = new FractionDelegateImpl(fm, f) {};
                //使用 IntelligentCache.KEY_KEEP 作为 key 的前缀, 可以使储存的数据永久存储在内存中
                //否则存储在 LRU 算法的存储空间中, 前提是 Fragment 使用的是 IntelligentCache (框架默认使用)
                cache.put(IntelligentCache.getKeyOfKeep(FractionDelegate.FRACTION_DELEGATE), fractionDelegate);
            }
        }
    }

    private FractionDelegate fetchFragmentDelegate(Fraction fraction) {
        if (fraction instanceof IFraction) {
            Cache<String, Object> cache = getCacheFromFragment((IFraction) fraction);
            return (FractionDelegate) cache.get(IntelligentCache.getKeyOfKeep(FractionDelegate.FRACTION_DELEGATE));
        }
        return null;
    }

    @NonNull
    private Cache<String, Object> getCacheFromFragment(IFraction fraction) {
        Cache<String, Object> cache = fraction.provideCache();
        Preconditions.checkNotNull(cache, "%s cannot be null on Fragment", Cache.class.getName());
        return cache;
    }

    @Override
    public void onAbilityStart(Ability ability) {

    }

    @Override
    public void onAbilityActive(Ability ability) {

    }

    @Override
    public void onAbilityInactive(Ability ability) {

    }

    @Override
    public void onAbilityForeground(Ability ability) {

    }

    @Override
    public void onAbilityBackground(Ability ability) {

    }

    @Override
    public void onAbilityStop(Ability ability) {

    }

    @Override
    public void onAbilitySaveState(PacMap pacMap) {

    }
}
